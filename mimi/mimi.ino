
#include "MIDIUSB.h"

////////////////////////////////////////////////////////////////////////////////

//#define USB_MANUFACTURER "Mimi"

#define FADER_PIN_1 A1
#define FADER_PIN_2 A2
#define FADER_PIN_3 A3
#define FADER_PIN_4 A4

////////////////////////////////////////////////////////////////////////////////

int delayTime = 50;
uint8_t midiChannel = 0;

////////////////////////////////////////////////////////////////////////////////

void midiNoteOn(byte channel, byte pitch, byte velocity)
{
	byte p1 = 0x09;
	byte p2 = 0x90 | channel;
	midiEventPacket_t noteOn = {p1, p2, pitch, velocity};
	MidiUSB.sendMIDI(noteOn);
}

void midiNoteOff(byte channel, byte pitch, byte velocity)
{
	byte p1 = 0x08;
	byte p2 = 0x80 | channel;
	midiEventPacket_t noteOff = {p1, p2, pitch, velocity};
	MidiUSB.sendMIDI(noteOff);
}

void midiControlChange(byte channel, byte control, byte value)
{
	byte p1 = 0x0B;
	byte p2 = 0xB0 | channel;
	midiEventPacket_t event = {p1, p2, control, value};
	MidiUSB.sendMIDI(event);
}

//void sendMidiControl() {

void setup()
{
	Serial.begin(115200);
}

void loop()
{

	/*
	Serial.println("Sending note on");
	noteOn(0, 10, 100);   // Channel 0, middle C, normal velocity
	MidiUSB.flush();
	delay(500);
	
	Serial.println("Sending note off");
	noteOff(0, 48, 64);  // Channel 0, middle C, normal velocity
	MidiUSB.flush();
	delay(1500);
	*/

	uint16_t fader1 = analogRead(FADER_PIN_1);
	uint16_t m1 = fader1 / 1024.0 * 128;

	midiControlChange(midiChannel, 1, m1);
	MidiUSB.flush();

	Serial.print(fader1);
	Serial.print(" ");
	Serial.println(m1);
	Serial.println(fader1);

	//byte v;
	//if( value == true ) v = 1;
	//else v = 0;

	/*
	byte channel = 1;
	byte control = 1;
	byte value = 0;
	Serial.println("Sending control");
	midiEventPacket_t note = {
		0x08,
		0x80 | channel,
		control,
		v
	};
	MidiUSB.sendMIDI(note);
	MidiUSB.flush();
	//delay(1500);
	*/

	delay(delayTime);
}

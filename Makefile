NAME = mimi
#BIN = $(NAME)/bin
INO = $(NAME)/$(NAME).ino
FQBN = arduino:avr:micro
PORT = /dev/ttyACM0

all: build

#$(BIN): $(INO)

build: $(BIN)
	arduino-cli compile --fqbn $(FQBN) $(NAME)
	#arduino-builder -compile $(INO) -hardware Arduino Micro
	#arduino --verify --pref build.path=$@ $<

upload: $(BIN)
	arduino-cli upload --fqbn $(FQBN) $(NAME) -p $(PORT)
	#arduino --upload --pref build.path=$(BIN) $(INO)

clean:
	rm -f mimi/*.elf
	rm -f mimi/*.hex

.PHONY: all build upload clean
